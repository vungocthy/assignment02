﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public class EmailRequirement: IAuthorizationRequirement
    {
        public string[] AllowedEmails { get; }

        public EmailRequirement(params string[] allowedEmails)
        {
            AllowedEmails = allowedEmails;
        }
    }
}

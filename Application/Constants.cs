﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class Constants
    {
        public static string AlertSuccess = "AlertSuccess";
        public static string AlertDanger = "AlertDanger";
        public static string AlertWarning = "AlertWarning";
    }
}

﻿using Application.ViewModels.Customers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IServices
{
    public interface ICustomerService
    {
        IEnumerable<CustomerDTO> GetCustomers();
        //IEnumerable<CustomerViewModel> GetCustomersViewModel();
        CustomerDTO GetCustomerById(int id);
        List<CustomerDTO> SearchCustomer(string search);
        bool AddNewCustomer(CustomerDTO newCustomer);
        bool DeleteCustomer(int id);
        bool UpdateCustomer(CustomerDTO customer);
    }
}

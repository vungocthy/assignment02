﻿using Application.ViewModels;
using Application.ViewModels.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IServices
{
    public interface ILoginService
    {
        CustomerDTO LoginCustomerCheck(LoginModel loginModel);
        bool LoginAdminCheck(LoginModel loginModel);
    }
}

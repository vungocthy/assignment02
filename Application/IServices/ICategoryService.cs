﻿using Application.ViewModels.CategoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IServices
{
    public interface ICategoryService
    {
        IEnumerable<string> GetCategoryNames();
        IEnumerable<CategoryDTO> GetCategoryDTOs();
    }
}

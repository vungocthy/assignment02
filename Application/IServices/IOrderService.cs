﻿using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IServices
{
    public interface IOrderService
    {
        IEnumerable<Order> GetOrders();
        Order GetOrderById(int id);
        //List<OrderCusDTO> GetOrderByCusId(int id);
        List<Order> GetOrderByCusId(int id);
        List<Order> SearchOrders(string search);
        bool AddNewOrder(Order orderDTO);
        bool DeleteOrder(Order order);
        bool UpdateOrder(Order orderDTO);
    }
}

﻿using Application.ViewModels.FlowerBouquet;
using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IServices
{
    public interface IFlowerBouquetService
    {
        IEnumerable<FlowerBouquetDTO> GetFlowerBouquets();
        List<FlowerBouquet> SearchFlowerBouquet(string search);
        FlowerBouquetDTO FlowerBouquetById(int id);
        bool AddNewFlower(FlowerBouquetDTO newFlower);
        bool DeleteFlower(int flowerID);
        bool UpdateFlower(FlowerBouquetDTO updateFlower);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.SuppilerModels
{
    public class SuppilerDTO
    {
        public int SupplierId { get; set; }
        public string? SupplierName { get; set; }
    }
}

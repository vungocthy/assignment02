﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.Customers
{
    public class CustomerDTO
    {
        public int CustomerId { get; set; }
        public string Email { get; set; } = null!;
        public string CustomerName { get; set; } = null!;
        public string City { get; set; } = null!;
        public string Country { get; set; } = null!;
        [DataType(DataType.Password)]
        public string Password { get; set; } = null!;
        public DateTime? Birthday { get; set; }
    }
}

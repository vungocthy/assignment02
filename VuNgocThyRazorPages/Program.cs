using Application.Utils;
using Application.ViewModels;
using Infrastructures;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServices(builder.Configuration.GetConnectionString("FUFlowerBouquetDB"));
/*builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("RequireEmail", policy =>
        policy.Requirements.Add(new EmailRequirement()));
});*/
/*builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("RequireAdminEmail", policy =>
        policy.Requirements.Add(new EmailRequirement("admin@FUflowerbouquet.com")));
});*/
/*builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("RequireEmail", policy =>
        policy.RequireAssertion(context =>
            context.User.HasClaim(c =>
                c.Type == ClaimTypes.Email && c.Value == "admin@FUflowerbouquet.com")));
});

builder.Services.Configure<CookieAuthenticationOptions>(options =>
{
    options.LoginPath = "/Login";
    options.AccessDeniedPath = "/Login";
});
*/
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(
                options =>
                {
                    options.AccessDeniedPath = "/Forbidden";
                    options.LogoutPath = "/";
                    options.LoginPath = "/";
                    options.SlidingExpiration = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(10);
                });
var app = builder.Build();


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();

﻿using Application;
using Application.IServices;
using Application.Utils;
using Application.ViewModels;
using Application.ViewModels.User;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Runtime.CompilerServices;
using System.Security.Claims;

namespace VuNgocThyRazorPages.Pages
{
    public class LoginPageModel : PageModel
    {
        private ILoginService _loginService;
        private UserLoginCurrent CurrentUser;
        public LoginPageModel(ILoginService loginService)
        {
            _loginService = loginService;
            CurrentUser = new UserLoginCurrent();
        }
        public string ActionResultErrorMessageText { get; set; }
        [BindProperty]
        public string Email { get; set; } = default;

        [BindProperty]
        public string Password { get; set; } = default;

        LoginModel loginModel;



        public IActionResult OnPost()
        {
            loginModel = new LoginModel
            {
                Email = Email,
                Password = Password,
            };
            bool flag = _loginService.LoginAdminCheck(loginModel);
            if (flag)
            {
                SignIn(Email, "Admin");
                CurrentUser.EmailCurrent = Email;
                return RedirectToPage("/AdminPages/AdminHomePage");
            }
            else if (flag == false)
            {

                var customer = _loginService.LoginCustomerCheck(loginModel);
                if (customer != null)
                {
                    SignIn(Email, "Customer");
                    CurrentUser.EmailCurrent = customer.Email;
                    return RedirectToPage("/CustomerPages/CustomerHomePage");
                }
                else
                {
                    TempData[Constants.AlertDanger] = "Wrong email or password.";
                }

            }
            else
            {
                TempData[Constants.AlertDanger] = "Wrong email or password.";

            }

            return Page();
        }

        private void SignIn(string email, string roleName)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, email),
                new Claim(ClaimTypes.Role, roleName)
            };


            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var authProperties = new AuthenticationProperties
            {

            };

            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity),
                authProperties);

        }


    }
}

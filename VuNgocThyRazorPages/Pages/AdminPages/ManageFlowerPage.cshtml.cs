using Application;
using Application.IServices;
using Application.ViewModels.FlowerBouquet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VuNgocThyRazorPages.Pages.AdminPages
{
    public class ManageFlowerPageModel : PageModel
    {
        private readonly IFlowerBouquetService _flowerBouquetService;
        private readonly ICategoryService _categoryService;
        public ManageFlowerPageModel(IFlowerBouquetService flowerBouquetService, ICategoryService categoryService)
        {
            _flowerBouquetService = flowerBouquetService;
            _categoryService = categoryService;

        }
        public List<FlowerBouquetDTO> flowerBouquetDTOs {  get; set; }
        public string CategoryName { get; set; }
        public string SuppillerName { get; set; }
        public void OnGet()
        {
            flowerBouquetDTOs=_flowerBouquetService.GetFlowerBouquets().ToList();
            
        }

        public IActionResult OnPost(int id)
        {
            bool flag = _flowerBouquetService.DeleteFlower(id);
            if(flag)
            {
                TempData[Constants.AlertSuccess] = "Delete flower bouquet is sucessfully";
            }
            else
            {
                TempData[Constants.AlertDanger] = "Delete flower bouquet is fail";
            }
            return Redirect("/AdminPages/ManageFlowerPage");
        }
    }
}

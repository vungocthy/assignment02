using Application.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VuNgocThyRazorPages.Pages.AdminPages
{
    public class UpdateFlowerPageModel : PageModel
    {
        private readonly IFlowerBouquetService _flowerBouquetService;
        public UpdateFlowerPageModel(IFlowerBouquetService flowerBouquetService)
        {
            _flowerBouquetService = flowerBouquetService;
        }
        public void OnGet(int id)
        {

        }
    }
}

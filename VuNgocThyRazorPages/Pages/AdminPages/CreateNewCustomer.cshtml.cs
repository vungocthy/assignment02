using Application;
using Application.IServices;
using Application.ViewModels.Customers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;


namespace VuNgocThyRazorPages.Pages.AdminPages
{
    public class CreateNewCustomerModel : PageModel
    {
        

        private ICustomerService _customerService;
        [BindProperty]
        public CustomerDTO NewCustomer { get; set; }
        public CreateNewCustomerModel(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        public void OnGet()
        {
        }
        public void OnPost()
        {
            bool flag=_customerService.AddNewCustomer(NewCustomer);
            if(flag)
            {
                TempData[Constants.AlertSuccess] = "Add New Customer Successfully";
            }
            else
            {
                TempData[Constants.AlertWarning] = "Add New Customer Fail.";
            }
        }
    }
}

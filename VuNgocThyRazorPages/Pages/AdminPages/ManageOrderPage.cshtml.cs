using Application.IServices;
using Application.ViewModels.Customers;
using Application.ViewModels.FlowerBouquet;
using Application.ViewModels.OrderModels;
using BussinessObjects.Models;
using Infrastructures.IRepositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VuNgocThyRazorPages.Pages.AdminPages
{
    public class ManageOrderPageModel : PageModel
    {
        /*private readonly IOrderService _orderService;
        private readonly ICustomerService _customerService;
        private readonly IFlowerBouquetService _flowerBouquetService;

        [BindProperty]
        public OrderDTO Order { get; set; }

        [BindProperty]
        public int CustomerId { get; set; }

        public List<CustomerDTO> Customers { get; set; }
        public List<FlowerBouquetDTO> FlowerBouquets { get; set; }

        public ManageOrderPageModel(IOrderService orderService, ICustomerService customerService, IFlowerBouquetService flowerBouquetService)
        {
            _orderService = orderService;
            _customerService = customerService;
            _flowerBouquetService = flowerBouquetService;
        }

        public void OnGetAsync()
        {
            Customers = ( _customerService.GetCustomers()).ToList();
            FlowerBouquets = (_flowerBouquetService.GetFlowerBouquets()).ToList();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Customers = (_customerService.GetCustomers()).ToList();
                FlowerBouquets = (_flowerBouquetService.GetFlowerBouquets()).ToList();
                return Page();
            }

            var customer = _customerService.GetCustomerById(CustomerId);
            var order = new Order
            {
                CustomerId = customer.CustomerId,
                OrderDate = DateTime.Now,
                Total = Order.Total,
                OrderStatus = "Pending"
            };
            var orderId = _orderService.AddNewOrder(order);

            // Check the quantity of the flower bouquet
            var flowerBouquet = _flowerBouquetService.FlowerBouquetById(OrderDetailDTO);
            if (flowerBouquet.UnitsInStock < Order.Quantity)
            {
                ModelState.AddModelError("Order.Quantity", "The quantity is not available.");
                Customers = (await _customerRepository.GetAllCustomers()).ToList();
                FlowerBouquets = (await _flowerBouquetRepository.GetAllFlowerBouquets()).ToList();
                return Page();
            }

            // Update the units in stock of the flower bouquet
            flowerBouquet.UnitsInStock -= Order.Quantity;
            await _flowerBouquetRepository.UpdateFlowerBouquet(flowerBouquet);

            // Add the order detail
            var orderDetail = new OrderDetail
            {
                OrderId = orderId,
                FlowerBouquetId = Order.FlowerBouquetId,
                UnitPrice = Order.UnitPrice,
                Quantity = Order.Quantity,
                Discount = 0
            };



          _orderService.AddOrderDetail(orderDetail);

            // Update the units in stock of the flower bouquet for the customer
            var customerFlowerBouquet = customer.Orders.FirstOrDefault(o => o.FlowerBouquetId == Order.FlowerBouquetId);
            if (customerFlowerBouquet != null)
            {
                customerFlowerBouquet.Quantity += Order.Quantity;
                customerFlowerBouquet.Total += Order.Quantity * Order.UnitPrice;
            }
            else
            {
                customer.Orders.Add(new OrderDTO
                {
                    FlowerBouquetId = Order.FlowerBouquetId,
                    Quantity = Order.Quantity,
                    UnitPrice = Order.UnitPrice,
                    Total = Order.Quantity * Order.UnitPrice
                });
            }
            await _customerRepository.UpdateCustomer(customer);

            return RedirectToPage("/ManageOrder");
        }*/
    }
}

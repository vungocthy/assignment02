using Application.IServices;
using Application.ViewModels.Customers;
using Application.ViewModels.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VuNgocThyRazorPages.Pages.AdminPages
{
    [Authorize(Roles = "Admin")]
    public class AdminHomePageModel : PageModel
    {
        private ICustomerService _customerService;
        [BindProperty]
        public List<CustomerDTO> listCustomerDTO { get; set; }
        public CustomerDTO NewCustomer { get; set; }
        public UserLoginCurrent CurrentUser {  get; set; }
        public AdminHomePageModel(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        public void OnGet()
        {
            
                listCustomerDTO = _customerService.GetCustomers().ToList();
            
        }

        public void OnPost()
        {
            bool flag= _customerService.AddNewCustomer(NewCustomer);
            if(flag)
            {
                ViewData["Message"] = "Add new customer successfully.";
            }
            else
            {
                ViewData["Message"] = "Add new customer fail.";
            }
        }
    }
}

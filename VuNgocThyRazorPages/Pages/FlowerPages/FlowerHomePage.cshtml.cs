using Application.IServices;
using Application.ViewModels.FlowerBouquet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VuNgocThyRazorPages.Pages.FlowerPages
{
    public class FlowerHomePageModel : PageModel
    {
        private readonly IFlowerBouquetService _fauquetService;
        public FlowerHomePageModel(IFlowerBouquetService flowerBouquetService)
        {
            _fauquetService = flowerBouquetService;
        }
        public List<FlowerBouquetDTO> flowerBouquetDTOs { get; set; }
        public void OnGet()
        {
            _fauquetService.GetFlowerBouquets().ToList();
        }
    }
}

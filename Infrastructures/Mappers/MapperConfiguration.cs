﻿using Application.ViewModels.Customers;
using Application.ViewModels.FlowerBouquet;
using Application.ViewModels.OrderModels;
using AutoMapper;
using BussinessObjects.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Mappers
{
    public class MapperConfiguration:Profile
    {
        public MapperConfiguration() 
        {
            CreateMap<CustomerDTO, Customer>().ReverseMap();
            CreateMap<FlowerBouquetDTO,FlowerBouquet>().ReverseMap();
            CreateMap<OrderDTO,Order>().ReverseMap();
            CreateMap<OrderDetailDTO, OrderDetail>().ReverseMap();
        }
    }
}

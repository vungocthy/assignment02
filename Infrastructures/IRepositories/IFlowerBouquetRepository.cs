﻿using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.IRepositories
{
    public interface IFlowerBouquetRepository
    {
        IEnumerable<FlowerBouquet> GetFlowerBouquets();
        FlowerBouquet GetFlowerBouquetById(int id);
        List<FlowerBouquet> SearchFlowerBouquet(string search);
        /*void AddNewFlower(FlowerBouquet flowerBouquet);
        void DeleteFlower(FlowerBouquet flowerBouquet);
        void SoftDelete(FlowerBouquet flowerBouquet);
        void UpdateFlower(FlowerBouquet flowerBouquet);*/
        bool AddNewFlower(FlowerBouquet flowerBouquet);
        bool DeleteFlower(FlowerBouquet flowerBouquet);
        bool SoftDelete(FlowerBouquet flowerBouquet);
        bool UpdateFlower(FlowerBouquet flowerBouquet);
        int findMaxIdFlower();
        string GetFlowerBouquetName(int flowerBouquetId);
    }
}

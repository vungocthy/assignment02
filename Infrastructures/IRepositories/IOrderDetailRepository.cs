﻿using BussinessObjects.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.IRepositories
{
    public interface IOrderDetailRepository
    {
        IEnumerable<OrderDetail> GetOrdersDetail();
        OrderDetail GetOrderDetailById(int orderId, int flowerbouquetId);
        List<OrderDetail> SearchOrderDetail(string search);
        bool AddNewOrderDetails(OrderDetail orderDetail);
        bool DeleteOrderDetails(OrderDetail orderDetail);
        bool UpdateOrderDetails(OrderDetail orderDetail);
        List<OrderDetail> GetOrderDetailByFlowerId(int flowerbouquetId);
        List<OrderDetail> GetOrderDetailByOrderId(int orderId);
    }
}

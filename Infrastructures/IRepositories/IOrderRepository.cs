﻿using BussinessObjects.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.IRepositories
{
    public interface IOrderRepository
    {
        IEnumerable<Order> GetOrders();
        Order GetOrderById(int id);
        List<Order> SearchOrders(string search);
        bool AddNewOrder(Order order);
        bool DeleteOrder(Order order);
        bool UpdateOrder(Order order);
        int findMaxId();
        List<Order> GetOrderByCusId(int id);
    }
}

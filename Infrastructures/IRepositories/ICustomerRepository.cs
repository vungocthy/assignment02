﻿using Application.ViewModels.Customers;
using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.IRepositories
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetCustomers();
        Customer GetCustomerById(int id);
        List<Customer> SearchCustomer(string search);
        bool AddNewCustomer(Customer customer);
        bool DeleteCustomer(Customer customer);
        bool UpdateCustomer(Customer customer);
        //Customer CustomerLogin(CusLoginDTO cusLogin);
        int findMaxId();
        Customer GetCustomerByEmail(string email);
    }
}

﻿using Application.ViewModels.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.IRepositories
{
    public interface ILoginRepository
    {
        CustomerDTO CustomerLogin(string email, string password);
    }
}

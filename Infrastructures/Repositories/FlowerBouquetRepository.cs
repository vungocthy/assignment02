﻿using Infrastructures.IRepositories;
using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class FlowerBouquetRepository:IFlowerBouquetRepository
    {
        private readonly FUFlowerBouquetManagementContext fuFlowerDb;
        public FlowerBouquetRepository(FUFlowerBouquetManagementContext fUFlowerBouquetManagementContext)
        {
            fuFlowerDb = fUFlowerBouquetManagementContext;
        }
        public IEnumerable<FlowerBouquet> GetFlowerBouquets() => fuFlowerDb.FlowerBouquets.Where(x=>x.FlowerBouquetStatus==1).ToList();
        public FlowerBouquet GetFlowerBouquetById(int id) => fuFlowerDb.FlowerBouquets.FirstOrDefault(x => x.FlowerBouquetId == id);
        public List<FlowerBouquet> SearchFlowerBouquet(string search)
        {
            List<FlowerBouquet> listFlowerBouquet = (from a in fuFlowerDb.FlowerBouquets
                                                     where a.FlowerBouquetId.ToString().Contains(search)
                                                          || a.CategoryId.ToString().Contains(search)
                                                          || a.FlowerBouquetName.Contains(search)
                                                          || a.Description.Contains(search)
                                                          || a.UnitPrice.ToString().Contains(search)
                                                          || a.UnitsInStock.ToString().Contains(search)
                                                          || a.FlowerBouquetStatus.Value.ToString().Contains(search)
                                                          || a.SupplierId.ToString().Contains(search)
                                                     select a).ToList();
            return listFlowerBouquet;
        }


        public bool AddNewFlower(FlowerBouquet flowerBouquet)
        {
            fuFlowerDb.FlowerBouquets.Add(flowerBouquet);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;

        }

        public bool DeleteFlower(FlowerBouquet flowerBouquet)
        {
            fuFlowerDb.FlowerBouquets.Remove(flowerBouquet);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }
        public bool SoftDelete(FlowerBouquet flowerBouquet)
        {
            flowerBouquet.FlowerBouquetStatus = 0;
            fuFlowerDb.Update(flowerBouquet);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }
        public bool UpdateFlower(FlowerBouquet flowerBouquet)
        {
            fuFlowerDb.FlowerBouquets.Update(flowerBouquet);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }

        public int findMaxIdFlower() => fuFlowerDb.FlowerBouquets.Max(x => x.FlowerBouquetId);

        public string GetFlowerBouquetName(int flowerBouquetId) => fuFlowerDb.FlowerBouquets.FirstOrDefault(x => x.FlowerBouquetId == flowerBouquetId).FlowerBouquetName;
    }
}

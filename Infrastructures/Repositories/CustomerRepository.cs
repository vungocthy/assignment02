﻿using Application.ViewModels.Customers;
using AutoMapper;
using Infrastructures.IRepositories;
using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CustomerRepository:ICustomerRepository
    {
        private readonly FUFlowerBouquetManagementContext fuFlowerDb;
        private IMapper _mapper;
        public CustomerRepository(FUFlowerBouquetManagementContext fUFlowerBouquetManagementContext,IMapper mapper)
        {
            fuFlowerDb = fUFlowerBouquetManagementContext;
            _mapper = mapper;
        }
        public IEnumerable<Customer> GetCustomers()
        {
            List<Customer> customers;
            try
            {
                customers = fuFlowerDb.Customers.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return customers;
        }

        public Customer GetCustomerById(int id)
        {
            Customer customer;
            try
            {
                customer = fuFlowerDb.Customers.FirstOrDefault(x => x.CustomerId == id);
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}", ex);
            }
            return customer;
        }

        public Customer GetCustomerByEmail(string email)
        {
            Customer customer;
            try
            {
                customer = fuFlowerDb.Customers.FirstOrDefault(x => x.Email == email);
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}", ex);
            }
            return customer;
        }
        public List<Customer> SearchCustomer(string search)
        {
            return (from c in fuFlowerDb.Customers
                    where c.CustomerId.ToString().Contains(search)
                           || c.CustomerName.Contains(search)
                           || c.Email.Contains(search)
                           || c.City.Contains(search)
                           || c.Country.Contains(search)
                           || c.Birthday.ToString().Contains(search)
                    select c).ToList();

        }
        public bool AddNewCustomer(Customer customer)
        {
            fuFlowerDb.Customers.Add(customer);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }
        public bool DeleteCustomer(Customer customer)
        {
            fuFlowerDb.Customers.Remove(customer);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }
        public bool UpdateCustomer(Customer customer)
        {
            fuFlowerDb.Customers.Update(customer);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }

        public int findMaxId() => fuFlowerDb.Customers.Max(x => x.CustomerId);

    }
}


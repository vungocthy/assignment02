﻿using Infrastructures.IRepositories;
using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class OrderRepository:IOrderRepository
    {
        private readonly FUFlowerBouquetManagementContext fuFlowerDb;
        public OrderRepository(FUFlowerBouquetManagementContext fUFlowerBouquetManagementContext)
        {
            fuFlowerDb = fUFlowerBouquetManagementContext;
        }
        public IEnumerable<Order> GetOrders() => fuFlowerDb.Orders.ToList();
        public Order GetOrderById(int id) => fuFlowerDb.Orders.FirstOrDefault(x => x.OrderId == id);
        public List<Order> GetOrderByCusId(int id) => fuFlowerDb.Orders.Where(x => x.CustomerId == id).ToList();
        public List<Order> SearchOrders(string search)
        {
            List<Order> listOrder = (from a in fuFlowerDb.Orders
                                     where a.OrderId.ToString().Contains(search)
                                          || a.CustomerId.ToString().Contains(search)
                                          || a.OrderDate.ToString().Contains(search)
                                          || a.ShippedDate.ToString().Contains(search)
                                          || a.Total.Value.ToString().Contains(search)
                                          || a.OrderStatus.Contains(search)
                                     select a).ToList();
            return listOrder;
        }

        public bool AddNewOrder(Order order)
        {
            fuFlowerDb.Orders.Add(order);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }

        public bool DeleteOrder(Order order)
        {
            fuFlowerDb.Orders.Remove(order);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }
        public bool UpdateOrder(Order order)
        {
            fuFlowerDb.Orders.Update(order);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true;
            return false;
        }
        public int findMaxId() => fuFlowerDb.Orders.Max(x => x.OrderId);
    }
}

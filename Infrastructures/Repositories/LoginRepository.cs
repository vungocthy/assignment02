﻿using Application.ViewModels.Customers;
using AutoMapper;
using BussinessObjects.Models;
using Infrastructures.IRepositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class LoginRepository:ILoginRepository
    {
        private readonly FUFlowerBouquetManagementContext fuFlowerDb;
        IMapper _mapper;
        //public static FUFlowerManagement fUFlowerManagement;
        public LoginRepository(FUFlowerBouquetManagementContext fUFlowerBouquetManagementContext, IMapper mapper)
        {
            fuFlowerDb = fUFlowerBouquetManagementContext;
            _mapper = mapper;
        }
        public CustomerDTO CustomerLogin(string email, string password)
        {
            Customer customer = fuFlowerDb.Customers.FirstOrDefault(x => x.Email.Equals(email) && x.Password.Equals(password));
            var mapperCus = _mapper.Map<CustomerDTO>(customer);
            return mapperCus;
        }
    }
}

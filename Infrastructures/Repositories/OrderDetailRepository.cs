﻿using Infrastructures.IRepositories;
using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class OrderDetailRepository:IOrderDetailRepository
    {
        private new readonly FUFlowerBouquetManagementContext fuFlowerDb;
        public OrderDetailRepository(FUFlowerBouquetManagementContext fUFlowerBouquetManagementContext)
        {
            fuFlowerDb = fUFlowerBouquetManagementContext;
        }
        public IEnumerable<OrderDetail> GetOrdersDetail() => fuFlowerDb.OrderDetails.ToList();
        public OrderDetail GetOrderDetailById(int orderId, int flowerbouquetId) => fuFlowerDb.OrderDetails.FirstOrDefault(x => x.OrderId == orderId
                                                                                    && x.FlowerBouquetId == flowerbouquetId);
        public List<OrderDetail> GetOrderDetailByFlowerId(int flowerbouquetId) => fuFlowerDb.OrderDetails.Where(x => x.FlowerBouquetId == flowerbouquetId).ToList();
        public List<OrderDetail> GetOrderDetailByOrderId(int orderId) => fuFlowerDb.OrderDetails.Where(x => x.OrderId == orderId).ToList();

        public List<OrderDetail> SearchOrderDetail(string search)
        {
            return (from o in fuFlowerDb.OrderDetails
                    where o.OrderId.ToString().Contains(search)
                        || o.FlowerBouquetId.ToString().Contains(search)
                            || o.UnitPrice.ToString().Contains(search)
                            || o.Quantity.ToString().Contains(search)
                            || o.Discount.ToString().Contains(search)
                    select o).ToList();
        }
        public bool AddNewOrderDetails(OrderDetail orderDetail)
        {
            fuFlowerDb.OrderDetails.Add(orderDetail);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true; return false;
        }

        public bool DeleteOrderDetails(OrderDetail orderDetail)
        {
            fuFlowerDb.OrderDetails.Remove(orderDetail);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true; return false;
        }
        public bool UpdateOrderDetails(OrderDetail orderDetail)
        {
            fuFlowerDb.OrderDetails.Update(orderDetail);
            int save = fuFlowerDb.SaveChanges();
            if (save > 0) return true; return false;
        }
    }
}

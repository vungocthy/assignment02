﻿using BussinessObjects.Models;
using Infrastructures.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class SupplierRepository:ISupplierRepository
    {

        private readonly FUFlowerBouquetManagementContext fuFlowerDb;
        public SupplierRepository(FUFlowerBouquetManagementContext fUFlowerBouquetManagementContext)
        {
            fuFlowerDb = fUFlowerBouquetManagementContext;
        }
        public IEnumerable<Supplier> GetSuppliers() => fuFlowerDb.Suppliers.ToList();
    }
}

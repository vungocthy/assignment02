﻿using BussinessObjects.Models;
using Infrastructures.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CategoryRepository:ICategoryRepository
    {
        private readonly FUFlowerBouquetManagementContext _fuFlowerDb;
        public CategoryRepository(FUFlowerBouquetManagementContext fUFlowerBouquetManagementContext)
        {
            _fuFlowerDb = fUFlowerBouquetManagementContext;
        }
        public IEnumerable<Category> GetCategories() => _fuFlowerDb.Categories.ToList();

        public List<string> GetCategoryName() => _fuFlowerDb.Categories.Select(x => x.CategoryName).ToList();
    }
}

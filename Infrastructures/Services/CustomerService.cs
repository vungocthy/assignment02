﻿using Application.IServices;
using Application.ViewModels.Customers;
using AutoMapper;
using Infrastructures.IRepositories;
using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Services
{
    public class CustomerService:ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private IMapper _mapper;

        public CustomerService(ICustomerRepository customerRepository,  IMapper mapper)
        {
            _customerRepository = customerRepository;
            
            _mapper = mapper;
        }

        public IEnumerable<CustomerDTO> GetCustomers()
        {
            List<Customer> customers;

            var mappers=_mapper.Map<List<CustomerDTO>>(_customerRepository.GetCustomers().ToList());
            return mappers;
        }
        /*public IEnumerable<CustomerViewModel> GetCustomersViewModel()
        {
            IEnumerable<CustomerDTO> customerList = _customerRepository.GetCustomers();
            List<CustomerViewModel> displays = customerList.Select(p => new CustomerViewModel
            {
                CustomerId = p.CustomerId,
                Email = p.Email,
                CustomerName = p.CustomerName,
                City = p.City,
                Country = p.Country,
                Birthday = p.Birthday,
            }).ToList();
            return displays;
        }*/

        public CustomerDTO GetCustomerById(int id) => _mapper.Map<CustomerDTO>(_customerRepository.GetCustomerById(id));
        public Customer GetCustomerByEmail(string email) => _customerRepository.GetCustomerByEmail(email);
        public List<CustomerDTO> SearchCustomer(string search)
        {
            List<CustomerDTO> listCus=default;
            try
            {
                var mapperCus = _mapper.Map<List<Customer>>(listCus);
                mapperCus = _customerRepository.SearchCustomer(search);
            }
            catch (Exception ex)
            {
                throw new Exception("Nothing to search", ex);
            }
            return listCus;

        }

        public bool AddNewCustomer(CustomerDTO newCustomer)
        {
            int maxID = _customerRepository.findMaxId();
            bool flag = false;
            if (GetCustomerByEmail(newCustomer.Email) == null && !newCustomer.Email.Equals("admin@FUflowerbouquet.com"))
            {
                flag = _customerRepository.AddNewCustomer(new Customer
                {
                    CustomerId = maxID + 1,
                    Email = newCustomer.Email,
                    CustomerName = newCustomer.CustomerName,
                    City = newCustomer.City,
                    Country = newCustomer.Country,
                    Password = "1",
                    Birthday = newCustomer.Birthday,
                });
            }
            return flag;
        }

        public bool DeleteCustomer(int id)
        {
            bool flag = false;
            try
            {
                var findCus = _customerRepository.GetCustomerById(id);
                if (findCus != null)
                {
                    flag = _customerRepository.DeleteCustomer(findCus);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Delete is fail", ex);
            }
            return flag;
        }

        public bool UpdateCustomer(CustomerDTO customer)
        {

            bool flag = false;
            try
            {
                var findCus = _customerRepository.GetCustomerById(customer.CustomerId);
                if (findCus != null)
                {
                    findCus.CustomerName = customer.CustomerName;
                    findCus.City = customer.City;
                    findCus.Country = customer.Country;
                    findCus.Birthday = customer.Birthday;
                    flag = _customerRepository.UpdateCustomer(findCus);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Delete is fail", ex);
            }
            return flag;
        }
        //public List<Order> LoadOrderofCus(string email) => _orderRepository.GetOrderByCusId(GetCustomerByEmail(email).CustomerId);
    }
}

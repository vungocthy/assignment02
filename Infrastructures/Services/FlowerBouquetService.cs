﻿using Application.IServices;
using Application.ViewModels.FlowerBouquet;
using AutoMapper;
using Infrastructures.IRepositories;
using BussinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Services
{
    public class FlowerBouquetService:IFlowerBouquetService
    {
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IMapper _mapper;
        public FlowerBouquetService(IFlowerBouquetRepository flowerBouquetRepository, IOrderDetailRepository orderDetailRepository, IMapper mapper)
        {
            _flowerBouquetRepository = flowerBouquetRepository;
            _orderDetailRepository = orderDetailRepository;
            _mapper = mapper;
        }

        public IEnumerable<FlowerBouquetDTO> GetFlowerBouquets()
        {
            IEnumerable<FlowerBouquetDTO> flowerBouquets;
            try
            {
                flowerBouquets = _flowerBouquetRepository.GetFlowerBouquets().Select(p => new FlowerBouquetDTO
                {
                    FlowerBouquetId = p.FlowerBouquetId,
                    CategoryId = p.CategoryId,
                    FlowerBouquetName = p.FlowerBouquetName,
                    Description = p.Description,
                    UnitPrice = p.UnitPrice,
                    UnitsInStock = p.UnitsInStock,
                    SupplierId = p.SupplierId,
                });
            }
            catch (Exception ex)
            {
                throw new Exception("Empty list flower bouquest.");
            }
            return flowerBouquets;
        }

        public FlowerBouquetDTO FlowerBouquetById(int id) => _mapper.Map<FlowerBouquetDTO>(_flowerBouquetRepository.GetFlowerBouquetById(id));
        public List<FlowerBouquet> SearchFlowerBouquet(string search)
        {
            List<FlowerBouquet> listFlowerBouquets;
            try
            {
                listFlowerBouquets = _flowerBouquetRepository.SearchFlowerBouquet(search);

            }
            catch (Exception ex) { throw new Exception("Empty list."); }
            return listFlowerBouquets;
        }

        public bool AddNewFlower(FlowerBouquetDTO newFlower)
        {
            int maxFlowerId = _flowerBouquetRepository.findMaxIdFlower();
            bool flag = false;
            try
            {
                flag = _flowerBouquetRepository.AddNewFlower(new FlowerBouquet
                {
                    FlowerBouquetId = maxFlowerId + 1,
                    CategoryId = newFlower.CategoryId,
                    FlowerBouquetName = newFlower.FlowerBouquetName,
                    Description = newFlower.Description,
                    UnitPrice = newFlower.UnitPrice,
                    UnitsInStock = newFlower.UnitsInStock,
                    FlowerBouquetStatus = 1,
                    SupplierId = newFlower.SupplierId,
                });
            }
            catch (Exception ex) { throw new Exception("Add flower is fail."); }
            return flag;
        }
        public bool DeleteFlower(int flowerID)
        {
            bool flag = false;
            //var map = _mapper.Map<FlowerBouquet>(delFlower);
            try
            {
                var findOrder = _orderDetailRepository.GetOrderDetailByFlowerId(flowerID);
                var findFlower = _flowerBouquetRepository.GetFlowerBouquetById(flowerID);
                //var findOrder = _orderDetailRepository.GetOrderDetailByFlowerId(flowerID);
                if (findOrder.Count==0)
                {
                    flag = _flowerBouquetRepository.DeleteFlower(findFlower);
                }
                else if (findOrder.Count != 0)
                {
                    flag = _flowerBouquetRepository.SoftDelete(findFlower);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex) { throw new Exception("Delete flower is fail."); }
            return flag;

        }

        public bool UpdateFlower(FlowerBouquetDTO updateFlower)
        {
            bool flag = false;
            try
            {
                var mapperDTO = _mapper.Map<FlowerBouquet>(updateFlower);
                var findOrder = _flowerBouquetRepository.GetFlowerBouquetById(mapperDTO.FlowerBouquetId);
                if (findOrder != null)
                {
                    _flowerBouquetRepository.UpdateFlower(new FlowerBouquet
                    {
                        FlowerBouquetId = updateFlower.FlowerBouquetId,
                        CategoryId = updateFlower.CategoryId,
                        FlowerBouquetName = updateFlower.FlowerBouquetName,
                        Description = updateFlower.Description,
                        UnitPrice = updateFlower.UnitPrice,
                        UnitsInStock = updateFlower.UnitsInStock,
                        SupplierId = updateFlower.SupplierId,
                    });
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Update flower is fail.");
            }
            return flag;
        }
    }
}

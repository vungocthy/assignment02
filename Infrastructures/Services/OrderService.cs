﻿using Application.IServices;
using AutoMapper;
using BussinessObjects.Models;
using Infrastructures.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Services
{
    public class OrderService:IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private IMapper _mapper;
        //private readonly IOrderDetailRepository _orderDetailRepository;

        public OrderService(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            //_orderDetailRepository = orderDetailRepository;
            _mapper = mapper;
        }

        public IEnumerable<Order> GetOrders() => _orderRepository.GetOrders().ToList();
        public Order GetOrderById(int id)
        {
            Order order;
            try
            {
                order = _orderRepository.GetOrderById(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Empty in list", ex);
            }
            return order;
        }

        public List<Order>? GetOrderByCusId(int id)
        {
            List<Order> orderList = default;
            //var mapperList = _mapper.Map<List<OrderCusDTO>>(orderList);
            try
            {
                if (_orderRepository.GetOrderByCusId(id) != null)
                {
                    orderList = _orderRepository.GetOrderByCusId(id);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Empty in list", ex);
            }
            return orderList;
        }
        public List<Order> SearchOrders(string search)
        {
            List<Order> listOrder;
            try
            {
                listOrder = _orderRepository.SearchOrders(search);
            }
            catch (Exception ex) { throw new Exception("Empty list."); }
            return listOrder;
        }

        public bool AddNewOrder(Order orderDTO)
        {
            //var mapper=_mapper.Map<Order>(orderDTO);
            int maxId = _orderRepository.findMaxId();

            bool flag = _orderRepository.AddNewOrder(new Order
            {
                OrderId = maxId + 1,
                CustomerId = orderDTO.CustomerId,
                OrderDate = DateTime.Now,
                ShippedDate = orderDTO.ShippedDate,
                Total = orderDTO.Total,
                OrderStatus = orderDTO.OrderStatus,
            });

            return flag;
        }
        public bool DeleteOrder(Order order)
        {
            bool flag = _orderRepository.DeleteOrder(order);
            //var listOrderDetail = _orderDetailRepository.GetOrderDetailByOrderId(order.OrderId);

            return flag;
        }

        public bool UpdateOrder(Order orderDTO)
        {
            //var mapper=_mapper.Map<Order>(orderDTO);
            //int maxId = _orderRepository.findMaxId();

            bool flag = _orderRepository.UpdateOrder(new Order
            {
                //OrderId = maxId + 1,
                //CustomerId = orderDTO.CustomerId,
                OrderDate = orderDTO.OrderDate,
                ShippedDate = orderDTO.ShippedDate,
                Total = orderDTO.Total,
                OrderStatus = orderDTO.OrderStatus,
            });

            return flag;
        }
    }
}

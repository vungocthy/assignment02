﻿using Application.IServices;
using Application.ViewModels.SuppilerModels;
using Infrastructures.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Services
{
    public class SuppilerService:ISuppilerService
    {
        private readonly ISupplierRepository _supplierRepository;
        public SuppilerService(ISupplierRepository supplierRepository)
        {
            _supplierRepository = supplierRepository;
        }
        public IEnumerable<SuppilerDTO> GetSupplierDTOs() => _supplierRepository.GetSuppliers().Select(p => new SuppilerDTO
        {
            SupplierId = p.SupplierId,
            SupplierName = p.SupplierName,
        }).ToList();
    }
}

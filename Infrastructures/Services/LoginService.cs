﻿using Application.IServices;
using Application.ViewModels;
using Application.ViewModels.Customers;
using Infrastructures.IRepositories;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Services
{
    public class LoginService:ILoginService
    {
        private readonly ILoginRepository _loginRepository;
		private IConfiguration _configuration;
		public LoginService(ILoginRepository loginRepository, IConfiguration configuration)
        {
            _loginRepository = loginRepository;
			/*var builder = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json");

			_configuration = builder.Build();
            */
            _configuration = configuration;
		}
       /* private static IConfiguration _configuration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", true, true)
    .Build();*/
        public CustomerDTO LoginCustomerCheck(LoginModel loginModel)=> _loginRepository.CustomerLogin(loginModel.Email, loginModel.Password);

        public bool LoginAdminCheck(LoginModel loginModel)
        {
            var adminEmail = _configuration["EmailSetting:Email"];
            var adminPass = _configuration["EmailSetting:Password"];
            if(adminEmail.Equals(loginModel.Email) && adminPass.Equals(loginModel.Password)) return true;
            return false;
        }
    }
}

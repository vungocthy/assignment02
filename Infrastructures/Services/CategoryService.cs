﻿using Application.IServices;
using Application.ViewModels.CategoryModels;
using Infrastructures.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Services
{
    public class CategoryService:ICategoryService
    {
        private readonly ICategoryRepository _cateRepository;
        public CategoryService(ICategoryRepository categoryRepository)
        {
            _cateRepository = categoryRepository;
        }
        public IEnumerable<CategoryDTO> GetCategoryDTOs() => _cateRepository.GetCategories().Select(p => new CategoryDTO
        {
            CategoryId = p.CategoryId,
            CategoryName = p.CategoryName,
        }).ToList();
        public IEnumerable<string> GetCategoryNames()
        {
            return _cateRepository.GetCategoryName();
        }
    }
}

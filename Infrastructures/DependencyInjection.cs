﻿using Application.IServices;
using Infrastructures.IRepositories;
using Infrastructures.Mappers;
using BussinessObjects.Models;
using Infrastructures.Repositories;
using Infrastructures.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Application.Utils;
using Microsoft.AspNetCore.Authorization;

namespace Infrastructures
{
    public static class DependencyInjection
    {
		public static IServiceCollection AddServices(this IServiceCollection services, string connectionString)
		{
			services.AddDbContext<FUFlowerBouquetManagementContext>(
				options => options.UseSqlServer(connectionString)
				);
            //services.AddDbContext<FUFlowerBouquetManagementContext>();

            services.AddSingleton<IAuthorizationHandler, EmailRequirementHandler>();
            services.AddAutoMapper(typeof(MapperConfiguration));

			services.AddScoped<ILoginRepository, LoginRepository>();
			services.AddScoped<ICustomerRepository, CustomerRepository>();
			services.AddScoped<IFlowerBouquetRepository, FlowerBouquetRepository>();
			services.AddScoped<IOrderRepository, OrderRepository>();
			services.AddScoped<IOrderDetailRepository, OrderDetailRepository>();
			services.AddScoped<ICategoryRepository, CategoryRepository>();
			services.AddScoped<ISupplierRepository, SupplierRepository>();
			

			services.AddScoped<ILoginService,LoginService>();
			services.AddScoped<ICustomerService,CustomerService>();
			services.AddScoped<IFlowerBouquetService, FlowerBouquetService>();
			services.AddScoped<IOrderService, OrderService>();
			services.AddScoped<IOrderDetailService, OrderDetailService>();
			services.AddScoped<ICategoryService, CategoryService>();
			services.AddScoped<ISuppilerService, SuppilerService>();

            return services;
		}
       
    }
}
